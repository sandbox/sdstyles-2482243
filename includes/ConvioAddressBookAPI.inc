<?php

/**
 * @file
 * Address Book - Convio Open API.
 */

class ConvioAddressBookAPI extends ConvioAPI {

  function __construct() {
    // Load convio configuration.
    $config = convioapi_get_configuration();
    $this->host = $config['convio_host'];
    $this->api_key = $config['convio_api_key'];
    $this->short_name = $config['convio_short_name'];
    $this->user_name = $config['convio_user_name'];
    $this->password = $config['convio_password'];
    $this->debug = $config['convio_debug'];

    $this->servlet = 'CRAddressBookAPI';

    $auth = new ConvioAuthAPI();
    $this->authToken = $auth->getAuthUserToken();
  }

  protected function addressBookCall($method, $param) {
    $param += array(
      'method' => $method,
      'sso_auth_token' => $this->authToken,
    );
    $response = parent::makeCall($this->servlet, $param);
    return parent::parseCallResponse($response);
  }

  public function addAddressBookGroup($group_name, $param = array()) {
    $param += array(
      'group_name' => $group_name,
    );
    $request = $this->addressBookCall(__FUNCTION__, $param);
    return $request;
  }

  public function addAddressBookContact($fname, $lname, $email, $param = array()) {
    $param += array(
      'first_name' => $fname,
      'last_name' => $lname,
      'email' => $email,
    );
    $request = $this->addressBookCall(__FUNCTION__, $param);
    return $request;
  }

  public function addAddressBookContacts($contacts, $param = array()) {
    foreach ($contacts as $contact) {
      $contacts_list[] = sprintf('"%" <%>, ', $contact['name'], $contact['email']);
    }
    $contacts_to_add = implode(',', $contacts_list);
    $param += array(
      'contacts_to_add' => $contacts_to_add,
    );
    $request = $this->addressBookCall(__FUNCTION__, $param);
    return $request;
  }

  public function addContactsToGroup($group_id, $contact_ids, $param = array()) {
    $contacts = implode(',', (array) $contact_ids);
    $param += array(
      'group_id' => $group_id,
      'contact_ids' => $contacts,
    );
    $request = $this->addressBookCall(__FUNCTION__, $param);
    return $request;
  }

  public function getAddressBookContacts($param = array()) {
    return $this->addressBookCall(__FUNCTION__, $param);
  }

  public function getAddressBookContactsByEmail($email, $param = array()) {
    $param += array(
      'list_filter_column' => 'email',
      'list_filter_text' => $email,
    );
    return $this->addressBookCall('getAddressBookContacts', $param);
  }

  public function getAddressBookGroups($param = array()) {
    return $this->addressBookCall(__FUNCTION__, $param);
  }

  public function deleteAddressBookContacts($contact_ids, $param = array()) {
    $contacts = implode(',', (array) $contact_ids);
    $param += array(
      'contact_ids' => $contacts,
    );
    return $this->addressBookCall(__FUNCTION__, $param);
  }

  public function deleteAddressBookGroups($group_ids, $param = array()) {
    $groups = implode(',', (array) $group_ids);
    $param += array(
      'group_ids' => $groups,
    );
    return $this->addressBookCall(__FUNCTION__, $param);
  }

  public function removeContactFromGroup($group_id, $contact_id, $param = array()) {
    $param += array(
      'group_id' => $group_id,
      'contact_id' => $contact_id,
    );
    return $this->addressBookCall(__FUNCTION__, $param);
  }

}
