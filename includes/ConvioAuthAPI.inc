<?php

/**
 * @file
 * Single Sign On - Convio Open API.
 */

class ConvioAuthAPI extends ConvioAPI {

  function __construct() {
    // Load convio configuration.
    $config = convioapi_get_configuration();
    $this->host = $config['convio_host'];
    $this->api_key = $config['convio_api_key'];
    $this->short_name = $config['convio_short_name'];
    $this->user_name = $config['convio_user_name'];
    $this->password = $config['convio_password'];
    $this->debug = $config['convio_debug'];

    $this->servlet = 'CRConsAPI';
  }

  public function authUser($param = array()) {
    $param += array(
      'method' => 'login',
      'login_name' => $this->login_name,
      'login_password' => $this->password,
      'user_name' => $this->user_name,
      'password' => $this->password,
    );

    $response = $this->makeCall($this->servlet, $param);

    $result = $this->parseCallResponse($response);

    return $result;
  }

  public function getAuthUserToken($param = array()) {
    $response = $this->authUser($param);
    if (isset($response['loginResponse']['token'])) {
      $token = $response['loginResponse']['token'];
    }
    else {
      // Register ome error.
      $token = NULL;
      drupal_set_message(t('Authentication failed.'), 'error');
    }

    return $token;
  }
}