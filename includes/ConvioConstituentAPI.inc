<?php

/**
 * @file
 * Constituent - Convio Open API.
 */

class ConvioConstituentAPI extends ConvioAPI {

  function __construct() {
    // Load convio configuration.
    $config = convioapi_get_configuration();
    $this->host = $config['convio_host'];
    $this->api_key = $config['convio_api_key'];
    $this->short_name = $config['convio_short_name'];
    $this->user_name = $config['convio_user_name'];
    $this->password = $config['convio_password'];
    $this->debug = $config['convio_debug'];

    $this->servlet = 'CRConsAPI';

    $auth = new ConvioAuthAPI();
    $this->authToken = $auth->getAuthUserToken();
  }

  public function create($param = array()) {
    $param += array(
      'sso_auth_token' => $this->authToken,
    );
    $request = parent::convioRequest($this->servlet, __FUNCTION__, $param);
    return $request;
  }

  public function createOrUpdate($param = array()) {
    $param += array(
      'sso_auth_token' => $this->authToken,
    );
    $request = parent::convioRequest($this->servlet, __FUNCTION__, $param);
    return $request;
  }

  public function getUser($param = array()) {
    $param += array(
      'sso_auth_token' => $this->authToken,
    );
    $request = parent::convioRequest($this->servlet, __FUNCTION__, $param);
    return $request;
  }

  public function getUserGroups($param = array()) {
    $param += array(
      'sso_auth_token' => $this->authToken,
    );
    $request = parent::convioRequest($this->servlet, __FUNCTION__, $param);
    return $request;
  }

  public function getUserCenters($param = array()) {
    $param += array(
      'sso_auth_token' => $this->authToken,
    );
    $request = parent::convioRequest($this->servlet, __FUNCTION__, $param);
    return $request;
  }

  public function getGroupMembers($param = array()) {
    $param += array(
      'sso_auth_token' => $this->authToken,
    );
    $request = parent::convioRequest($this->servlet, __FUNCTION__, $param);
    return $request;
  }

  public function listCenters($param) {
    $param += array(
      'sso_auth_token' => $this->authToken,
    );
    $request = parent::convioRequest($this->servlet, __FUNCTION__, $param);
    return $request;
  }

  public function listUserFields($param) {
    $param += array(
      'sso_auth_token' => $this->authToken,
    );
    $request = parent::convioRequest($this->servlet, __FUNCTION__, $param);
    return $request;
  }
}
