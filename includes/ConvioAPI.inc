<?php

/**
 * @file
 * Convio Open API.
 */

abstract class ConvioAPI {

  public $host;

  public $own_host;

  public $short_name;

  public $api_key;

  public $v = '1.0';

  public $servlet;

  public $response_format = 'json';

  public $login_name = NULL;

  public $login_password = NULL;

  public $user_name = NULL;

  public $password = NULL;

  public $debug = FALSE;

  private function getURL($servlet) {
    $url = sprintf('https://%s/%s/site/%s', $this->host, $this->short_name, $servlet);

    // @todo check host for private domain.s
    $url = sprintf('https://%s/site/%s', $this->host, $servlet);

    return $url;
  }

  private function buildQuery($query) {
    $query += array(
      'api_key' => $this->api_key,
      'v' => $this->v,
      'response_format' => $this->response_format,
    );
    return drupal_http_build_query($query);
  }

  public function makeCall($servlet, $param) {
    $url = $this->getURL($servlet);
    $post_data = $this->buildQuery($param);

    if (function_exists('curl_exec') === FALSE) {
      // Drupal http request.
      // @todo to tested.
      $response = drupal_http_request($url, $param);

      if ($response == '') {
        $response = sprintf("Error");
      }
    }
    else {
      $curl = curl_init();
      curl_setopt($curl, CURLOPT_URL, $url);
      curl_setopt($curl, CURLOPT_POST, TRUE);
      curl_setopt($curl, CURLOPT_POSTFIELDS, $post_data);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
      curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
      curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
      $response = curl_exec($curl);

      if ($response == '') {
        $response = sprintf("cURL Error %s: %s\n", curl_errno($curl), curl_error($curl));
      }

      curl_close($curl);
    }

    return $response;
  }

  public function responseDecode($response) {
    if (is_object($response)) {
      return (array) $response;
    }
    if ($this->response_format == 'json') {
      return drupal_json_decode($response);
    }
    return NULL;
  }

  public function parseCallResponse($response) {
    $response = $this->responseDecode($response);

    if (isset($response['errorResponse'])) {
      if ($this->debug) {
        drupal_set_message('Error: ' . $response['errorResponse']['code'] . ' - ' . $response['errorResponse']['message'], 'error');
        watchdog('ConvioAPI', 'Error: ' . $response['errorResponse']['code'] . ' - ' . $response['errorResponse']['message']);
      }
      else {
        drupal_set_message($response['errorResponse']['message'], 'error');
      }
    }

    return $response;
  }

  public function convioRequest($servlet, $method, $param = array()) {
    $param += array(
      'method' => $method,
    );
    $response = $this->makeCall($servlet, $param);
    return $this->parseCallResponse($response);
  }
}
