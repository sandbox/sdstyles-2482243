<?php

/**
 * @file
 * ConvioAPI backend settings.
 */

/**
 * ConvioAPI configuration form.
 *
 * @return mixed
 *   System configuration.
 */
function convioapi_admin_settings_form() {
  $form = array();

  $form['convio'] = array(
    '#title' => t('Open ConvioAPI settings'),
    '#type' => 'fieldset',
  );
  $form['admin_account'] = array(
    '#title' => t('ConvioAPI Administrative account'),
    '#type' => 'fieldset',
  );
  $form['admin_debug'] = array(
    '#title' => t('Debug'),
    '#type' => 'fieldset',
  );
  $form['convio']['convio_host'] = array(
    '#title' => t('Host url'),
    '#type' => 'textfield',
    '#default_value' => variable_get('convio_host'),
    '#description' => t('The base URL for the API calls. Different Convio customers have different base URLs (e.g., secure2.convio.net or secure3.convio.net or custom url).'),
  );
  $form['convio']['convio_short_name'] = array(
    '#title' => t('Name of organization'),
    '#type' => 'textfield',
    '#default_value' => variable_get('convio_short_name'),
    '#description' => t('Needed only for standard Convio url e.g. "secure2.convio.net".'),
  );
  $form['convio']['convio_api_key'] = array(
    '#title' => t('API Key'),
    '#type' => 'textfield',
    '#default_value' => variable_get('convio_api_key'),
    '#description' => t('Convio access API Key'),
  );
  $form['convio']['convio_source_code'] = array(
    '#title' => t('Original Source Code'),
    '#type' => 'textfield',
    '#default_value' => variable_get('convio_source_code', ''),
    '#description' => t('Constituent "Origin Source Code" and "Origin Subsource Code".'),
  );
  $form['admin_account']['convio_user_name'] = array(
    '#title' => t('User Name'),
    '#type' => 'textfield',
    '#default_value' => variable_get('convio_user_name'),
    '#description' => t('Special administrative account used just for API access.'),
  );
  $form['admin_account']['convio_password'] = array(
    '#title' => t('Password'),
    '#type' => 'textfield',
    '#default_value' => variable_get('convio_password'),
    '#description' => t('Password for administrative account.'),
  );
  $form['admin_debug']['convio_debug'] = array(
    '#title' => t('Debug'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('convio_debug'),
    '#description' => t('Enable for more debug options.'),
  );
  $link = l('here', 'open-convioapi/authtest', array('attributes' => array('target' => '_blank')));
  $form['admin_debug']['convio_authtest'] = array(
    '#markup' => t('Check authentication access !here.', array('!here' => $link)),
  );

  return system_settings_form($form);
}
